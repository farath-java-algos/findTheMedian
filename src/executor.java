/**
 * Created by farath.shba on 4/11/2017.
 */
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;

public class executor
{
    public static double findTheMedian(String strOfNums, int size)
    {
        String[] numStr = strOfNums.split(" ");
        ArrayList<Integer> numArr = new ArrayList<Integer>();

        //  Conversion of String to Integer type
        for(int k = 0; k < numStr.length; k++)
        {
            numArr.add(Integer.parseInt(numStr[k]));
            System.out.print(" << " + numStr[k]);
        }
        System.out.println();

        //  Sort the ArrayList in ascending order
        Collections.sort(numArr);

        //  Check if the length of the Array is odd or even

        for(int i = 0; i < numArr.size(); i ++)
        {
            System.out.print(" >> " + numArr.get(i));

        }

        System.out.println();
        if(numArr.size() % 2 != 0 && numArr.size() > 1)
        {
            //  Odd
            return numArr.get(numArr.size() / 2);
        }
        else
        {
            //  Even
            double tmp = (numArr.get(numArr.size() / 2 )) + (numArr.get(numArr.size() / 2 ) -1);
            return tmp / 2.0;
        }
    }

    public static void main(String[] args)
    {
        int size = new Scanner(System.in).nextInt();
        String numbers = new Scanner(System.in).nextLine();

        System.out.println("Median >> " + findTheMedian(numbers, size));
    }
}
